use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;
use std::env;

mod model;
mod db;
use db::DB;

#[get("/")]
async fn hello(database: web::Data<DB>) -> impl Responder {
    let data = database.find_all().await;

    match data {
        Ok(data) => HttpResponse::Ok().json(&data),
        _ => panic!("Error")
    }
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

#[actix_web::main]
async fn main()  -> std::io::Result<()> {
    dotenv().ok();
    env_logger::init();

    let application_port = match env::var("APPLICATION_PORT") {
        Ok(port) => port,
        _ => "8080".to_string()
    };

    let server = format!("0.0.0.0:{}", application_port);

    let db = DB::init("brands").await ;
    
    let database = match db {
        Ok(db) => db,
        _ => panic!("DB Error")
    };

    HttpServer::new(move || {
        App::new()
         .data(database.clone()) 
         .service(hello)
    })
    .bind(&server)?
    .run()
    .await
}

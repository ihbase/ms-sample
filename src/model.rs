use mongodb::bson::{document::Document};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Brand {
  pub sku: String
}

impl Brand {
  pub fn new(item: &Document) -> std::io::Result<Brand> {
    let sku = item.get_str("sku").expect("SKU field not found");

    let brand = Brand {
      sku: sku.to_owned()
    };

    Ok(brand)
  }
}
use futures::StreamExt;
use mongodb::{error::Error, Client, Collection};
use crate::{model::Brand};
use std::env;

#[derive(Clone, Debug)]
pub struct DB {
  collection: Collection
}

impl DB {
  pub async fn init(collection: &str) -> Result<DB, Error> {
    let db_url = env::var("MONGO_URI").expect("MONGO_URI value is mandatory");
    let db_name = env::var("DATABASE_NAME").expect("DATABASE_NAME value is mandatory");

    let client = Client::with_uri_str(&db_url).await?;
    let db = client.database(&db_name);

    Ok(DB { collection: db.collection(collection) })
  }

  pub async fn find_all(&self) -> Result<Vec<Brand>, Error>{
    let mut cursor = self.collection.find(None, None).await.unwrap();

    let mut data:Vec<Brand> = Vec::new();
    
    while let Some(item) = cursor.next().await {
      match item {
        Ok(doc) => data.push(Brand::new(&doc)?),
        _ => {}
      }
    }

    Ok(data)
  } 
}